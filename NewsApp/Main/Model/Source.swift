//
//  Source.swift
//  NewsApp
//
//  Created by Busazhida on 2/12/21.
//

import Foundation

public struct Source : Codable{
    var id: String? = nil
    var name: String
}
