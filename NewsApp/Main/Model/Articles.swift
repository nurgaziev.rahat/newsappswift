//
//  Articles.swift
//  NewsApp
//
//  Created by Busazhida on 2/12/21.
//

import Foundation
import ObjectMapper


class Articles: Mappable {
    var author: String?
    var title: String?
    var description: String?
    var url: String?
    var urlToImage: String?
    var publishedAt: String?
    var content: String?
    
    required convenience init?(map: Map){
        self.init()
    }
    
     func mapping(map: Map){
        author <- map["author"]
        description <- map["description"]
        title <- map["title"]
        urlToImage <- map["urlToImage"]
        url <- map["url"]
        publishedAt <- map["publishedAt"]
        
    }
}
