//
//  CategoryModel.swift
//  NewsApp
//
//  Created by Busazhida on 2/11/21.
//

import Foundation

struct CategoryModel {
    var title: String
    var image: String
}
