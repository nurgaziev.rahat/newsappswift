//
//  ViewController.swift
//  NewsApp
//
//  Created by Busazhida on 2/11/21.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    private var currentCategory = "business"
    private lazy var viewModel: MainViewModel = {
        return MainViewModel(delegate: self)
    }()
    
    private let layout = UICollectionViewFlowLayout()
    private let newsLayout = UICollectionViewFlowLayout()
    
    private lazy var categoryCV: UICollectionView = {
        layout.scrollDirection = .horizontal
        let view = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        view.dataSource = self
        view.delegate = self
        view.isScrollEnabled = true
        view.backgroundColor = .clear
        view.showsHorizontalScrollIndicator = false
        view.register(CategoryCell.self, forCellWithReuseIdentifier: CategoryCell.cellID)
        return view
    }()
    
    private lazy var newsCV: UICollectionView = {
        newsLayout.scrollDirection = .horizontal
        let view = UICollectionView(frame: self.view.frame,collectionViewLayout: newsLayout)
        view.dataSource = self
        view.delegate = self
        view.isScrollEnabled = false
        view.showsHorizontalScrollIndicator = false
        view.register(NewsCell.self, forCellWithReuseIdentifier: NewsCell.cellID)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(categoryCV)
        view.addSubview(newsCV)
        viewModel.getNewsByCategory(category: currentCategory)
        navigationItem.title = "NewsApp"
        navigationController?.navigationBar.prefersLargeTitles = true
        categoryCV.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.safeArea.top).offset(10)
            make.leading.trailing.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.1)
        }
        
        newsCV.snp.makeConstraints { (make) in
            make.top.equalTo(categoryCV.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.9)
        }
    }
}


extension ViewController: NewsViewModelDelegte{
    func reloadData() {
        newsCV.reloadData()
    }
}

extension ViewController: NewsCellDeleagte {
    func onNewsClick(newsUrl: String) {
        let detailNewsController = NewsDetailViewController()
        detailNewsController.newsUrl = newsUrl
        navigationController?.pushViewController(detailNewsController, animated: true)
    }
    
    
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == categoryCV {
            return CGSize(width: collectionView.frame.width/3, height: collectionView.frame.height)
        }else{
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        } 
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoryCV {
            return viewModel.categories.count
        }else{
            return viewModel.categories.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == categoryCV {
            newsCV.scrollToItem(at: indexPath, at: .left, animated: true)
            currentCategory = viewModel.categories[indexPath.row].title
            viewModel.getNewsByCategory(category: currentCategory)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == categoryCV {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCell.cellID, for: indexPath) as! CategoryCell
            cell.fill(viewModel.categories[indexPath.item])
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NewsCell.cellID, for: indexPath) as! NewsCell
            cell.fill(viewModel.news)
            cell.setupDelegate(delegate: self)
            return cell
        }
        
    }
    

}

