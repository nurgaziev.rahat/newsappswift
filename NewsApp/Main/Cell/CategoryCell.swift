//
//  CategoryCell.swift
//  NewsApp
//
//  Created by Busazhida on 2/11/21.
//

import Foundation
import UIKit
import SnapKit


class CategoryCell: BaseCVCell  {
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.layer.cornerRadius = 16
        view.layer.masksToBounds = true
        view.addBlur(0.6)
        return view
    }()
    
    private lazy var title: UILabel = {
        let view = UILabel()
        view.backgroundColor = .clear
        view.textColor = .white
        view.font = UIFont.systemFont(ofSize: 15,weight: .medium)
        return view
    }()
    
    override func addSubViews() {
        addSubview(image)
        image.addSubview(title)
        
    }
    
    override func setupUI() {
        image.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.width.height.equalToSuperview()
        }
        
        title.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
    }
    
    func fill(_ data: CategoryModel){
        title.text = data.title.uppercased()
        image.image = UIImage(named: data.image)
    }
}
