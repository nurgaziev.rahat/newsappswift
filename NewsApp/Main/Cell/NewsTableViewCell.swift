//
//  NewsTableViewCell.swift
//  NewsApp
//
//  Created by Busazhida on 2/14/21.
//

import Foundation
import UIKit
import SnapKit
import Kingfisher

class NewsTableViewCell: BaseTVCell {
    
    private lazy var newsImage: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = .gray
        return view
    }()
    
    private lazy var newsTitle: UILabel = {
        let view = UILabel()
        view.text = "Title"
        view.textColor = .black
        view.lineBreakMode = .byTruncatingTail
        view.numberOfLines = 2
        view.font = UIFont.boldSystemFont(ofSize: CGFloat(13))
        return view
    }()
    
    private lazy var newsDesription: UILabel = {
        let view = UILabel()
        view.text = "Description"
        view.textColor = .gray
        view.font = UIFont.boldSystemFont(ofSize: CGFloat(10))
        view.lineBreakMode = .byWordWrapping
        view.numberOfLines = 3
        return view
    }()
    
    override func addSubViews() {
        addSubview(newsImage)
        addSubview(newsTitle)
        addSubview(newsDesription)
    }
    
    override func setupUI() {
        newsImage.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(10)
            make.height.equalToSuperview().inset(10)
            make.width.equalToSuperview().multipliedBy(0.4)
        }
        newsTitle.snp.makeConstraints { (make) in
            make.top.equalTo(newsImage.snp.top)
            make.left.equalTo(newsImage.snp.right).offset(10)
            make.width.equalToSuperview().multipliedBy(0.55)
            make.height.equalToSuperview().multipliedBy(0.3)
        }
        newsDesription.snp.makeConstraints { (make) in
            make.top.equalTo(newsTitle.snp.bottom).offset(1)
            make.left.equalTo(newsTitle.snp.left)
            make.width.equalToSuperview().multipliedBy(0.55)
            make.height.equalToSuperview().multipliedBy(0.7)
        }
    }
    
    func fill(_ news: Articles){
        if let urlString = news.urlToImage {
            let urlImage =  URL(string: urlString)
            newsImage.kf.setImage(with: urlImage)
        }
        newsTitle.text = news.title
        newsDesription.text = news.description
    }
}
