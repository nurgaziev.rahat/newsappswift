//
//  NewsCell.swift
//  NewsApp
//
//  Created by Busazhida on 2/12/21.
//

import Foundation
import UIKit
import SnapKit

protocol NewsCellDeleagte : class {
    func onNewsClick(newsUrl: String)
}

class NewsCell: BaseCVCell {
    private weak var delegate: NewsCellDeleagte?
    
    private var news: [Articles] = []
    private lazy var newsTable: UITableView = {
        let view = UITableView()
        view.dataSource = self
        view.delegate = self
        view.isScrollEnabled = true
        view.register(NewsTableViewCell.self, forCellReuseIdentifier: NewsTableViewCell.cellID)
        return view
    }()
    
    func setupDelegate(delegate: NewsCellDeleagte){
        self.delegate = delegate
    }

    
    override func addSubViews() {
        contentView.addSubview(newsTable)
    }
    
    override func setupUI() {
        newsTable.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    func fill(_ data: [Articles]){
        self.news = data
        newsTable.reloadData()
    }
}

extension NewsCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.onNewsClick(newsUrl: news[indexPath.row].url ?? "empty")
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsTableViewCell.cellID,for: indexPath) as! NewsTableViewCell
        cell.fill(news[indexPath.row])
        return cell
    }
    
    
}
