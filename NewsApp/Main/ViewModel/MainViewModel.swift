//
//  MainViewModel.swift
//  NewsApp
//
//  Created by Busazhida on 2/11/21.
//

import Foundation
import Alamofire
import ObjectMapper


protocol NewsViewModelDelegte: class {
    func reloadData()
}
class MainViewModel {
    var categories = [
        CategoryModel(title: "business",image: "business"),
        CategoryModel(title: "entertainment",image: "entertainment"),
        CategoryModel(title: "general",image: "general"),
        CategoryModel(title: "health",image: "health"),
        CategoryModel(title: "science",image: "science"),
        CategoryModel(title: "sports",image: "sports"),
        CategoryModel(title: "technology",image: "technology"),
    ]
    var news: [Articles] = []
    
    private weak var delegate: NewsViewModelDelegte?
    
    init(delegate: NewsViewModelDelegte) {
        self.delegate = delegate
    }
    
    func getNews(){
        AF.request("https://newsapi.org/v2/top-headlines?country=ru&apiKey=97bc4a013b8a462faae2fde3b68b204e")
            .responseJSON(completionHandler: { (response) in
                if let json = response.value as? [String: Any]{
                    if let newsModel = json["articles"] as? Array<[String: Any]>{
                        for item in newsModel {
                            if let news = Mapper<Articles>().map(JSON: item){
                                self.news.append(news)
                                self.delegate?.reloadData()
                            }
                        }
                    }
                }
            })
    }
    
    func getNewsByCategory(category: String){
        self.news.removeAll()
        AF.request("https://newsapi.org/v2/top-headlines?country=ru&category=\(category)&apiKey=97bc4a013b8a462faae2fde3b68b204e")
            .responseJSON { (response) in
                if let json = response.value as? [String: Any]{
                    if let newsModel = json["articles"] as? Array<[String: Any]>{
                        for item in newsModel {
                            if let news = Mapper<Articles>().map(JSON: item){
                                self.news.append(news)
                            }
                        }
                        self.delegate?.reloadData()
                    }
                }
            }
    }
    
}
