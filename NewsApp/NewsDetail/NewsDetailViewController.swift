//
//  NewsDetailViewController.swift
//  NewsApp
//
//  Created by Busazhida on 2/14/21.
//

import UIKit
import SnapKit
import WebKit

class NewsDetailViewController: UIViewController {
    var newsUrl: String?
    
    private lazy var webView: WKWebView = {
        let view = WKWebView()
        view.navigationDelegate = self
        view.backgroundColor = .clear
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(webView)
        
        webView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.safeArea.top)
            make.bottom.equalTo(self.view.safeArea.bottom)
            make.leading.trailing.equalToSuperview()
        }
        if let news = newsUrl {
            webView.load(news)
        }
    }
}

extension NewsDetailViewController : WKNavigationDelegate{
    
}
